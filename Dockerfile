FROM --platform=${BUILDPLATFORM} golang:1.21 AS build
ENV CGO_ENABLED=0
ENV SENTRY_DSN=""

ARG GO_BUILD_LDFLAG
ARG BUILDPLATFORM

WORKDIR /build

# Cache dependencies
COPY go.mod ./
COPY go.sum ./
RUN --mount=type=cache,target=/go/pkg go mod download -x

# Add the sources and proceed with build
COPY main.go main.go

ARG TARGETARCH
ARG TARGETOS

RUN ["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -o error-tracking-test main.go"]

FROM gcr.io/distroless/static-debian11
COPY --from=build /build/error-tracking-test /
ENTRYPOINT ["/error-tracking-test", "fake-url"]
